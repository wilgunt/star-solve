import unittest
import sys
sys.path.append("..\src")

from src.StarSolveEngine.CoreFeatures.Parser.Parser import *

class TestParserEQ(unittest.TestCase):

    def setUp(self):
        self._Parser = ParseRawData()

    def test_DataHasFunctionsAndVariables_ListAndDictionaryCorrect(self):
        self._Parser.Clear()
        data = "var(x=4) var(y = 3) #linalg(this is the data i expect)#"
        
        self._Parser.ParseForFunctionAndVariableData(data)
        
        assert self._Parser.GetFunctionList() == [('linalg', 'this is the data i expect')]
        assert self._Parser.GetVariableDictionary() == {'x': '4', 'y': '3'}
    
    def test_DataHasFunctionsMultiLine_ListCorrect(self):
        self._Parser.Clear()
        data = "#linalg(\r\n"\
            "this is the data i expect\r\n"\
            ")#"
        
        self._Parser.ParseForFunctionAndVariableData(data)
        print(self._Parser.GetFunctionList())
        print(data)
        
        assert self._Parser.GetFunctionList() == [('linalg', '\r\nthis is the data i expect\r\n')]

if __name__ == '__main__':
    unittest.main()

