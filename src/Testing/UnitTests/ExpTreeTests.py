import unittest
import sys
sys.path.append("..\src")

from src.StarSolveEngine.CoreFeatures.ExpressionTree.StringToPostfix import *
from src.StarSolveEngine.CoreFeatures.ExpressionTree.PostFixToTree import *


class TestExpressionTree(unittest.TestCase):
    def test_PostFixIsValid_ReturnsExpectedTree(self):
        input = "1+2+3+4"
        converter = StringToPostFix()
        actual = converter.stringToPostfix(input)
        expected = ['1','2','+','3','+','4','+']
        assert expected == actual

    def test_PostFixIsInvalid_ReturnsNull(self):
        input = ""
        converter = StringToPostFix()
        actual = converter.stringToPostfix(input)
        expected = []
        assert expected == actual

    def test_PostFixIsValid_ReturnsExpectedResult(self):
        input = ['1','2','+','3','+','4','+']
        handler = PostFixToTree()
        expected = 10
        actual = handler.handle(input).Eval()
        print(actual)
        assert expected == actual

    def test_StringIsValid_Hard_ReturnsExpectedTree(self):
        input = "(1+99)*(7^4)+6-128"
        converter = StringToPostFix()
        actual = converter.stringToPostfix(input)
        expected = ['1', '99', '+', '7', '4', '^', '*', '6', '+', '128', '-']
        assert expected == actual

    def test_PostfixIsValid_Hard_ReturnsExpectedResult(self):
        input = ['1', '8', '^', '2', '*', '6', '+']
        handler = PostFixToTree()
        expected = 8
        actual = handler.handle(input).Eval()
        assert expected == actual


if __name__ == '__main__':
    unittest.main()

