import importlib

from StarSolveEngine.CoreFeatures.Parser.Parser import ParseRawData
from StarSolveEngine.CoreFeatures.ExpressionTree.StringToPostfix import StringToPostFix
from StarSolveEngine.CoreFeatures.ExpressionTree.PostFixToTree import PostFixToTree

        
def isDigitOrOperator(item):
    if str(item).isdigit():
        return True
    elif str(item) == '+' or str(item) == '-' or str(item) == '*' or str(item) == '/' or str(item) == '^':
        return True
    else:
        return False
    
    
class StarSolveEngine():

    rawData = ""
    _Parser = ParseRawData()
    variableDictionary = {}
    functionList = ()
    outputTexts = []
    mode = ""

    def setData(self,rawData):
        self.rawData = rawData

    def parseData(self):
        self._Parser.ParseForFunctionAndVariableData(self.rawData)
        self.variableDictionary = self._Parser.GetVariableDictionary()
        self.functionList = self._Parser.GetFunctionList()

    def clearEngineData(self):
        self.rawData = ""
        self.variableDictionary = {}
        self.functionList = ()
        self.outputTexts = []
        self.mode = ""

    def setMode(self, newMode):
        self.mode = newMode

    def solveSystem(self):
        line = 1
        items = self.variableDictionary.items()

        for item in items:
            exp = str(item[1])
            converter = StringToPostFix()
            result = converter.stringToPostfix(exp)
            result = self.replaceVariableValues(result)
            solver = PostFixToTree()

            try:
                result = solver.handle(result).Eval()
                self.variableDictionary[item[0]] = str(result)
            except:
                for x in result:
                    if isDigitOrOperator(x) != True:
                        text = "ERROR on line " + str(line) + ": "
                        text = text + str(x) + " is an undefined variable or function"
                        self.outputTexts.append(text)
                        return False

            line += 1

        return True
                
    def replaceVariableValues(self, postfix):
        i = 0
        for item in postfix:
            if item.isdigit():
                pass
            elif item == '+' or item == '-' or item == '*' or item == '/' or item == '^':
                pass
            else:
                try:
                    postfix[i] = self.variableDictionary[item]
                except:
                    print("error replacing variable")
            i += 1

        return postfix

    def run(self):
        if(self.mode == "Basic"):
            self.parseData()
            result = self.solveSystem()
            if result == True:
                for item in self.variableDictionary:
                    self.outputTexts.append(str(item) + "=" + str(self.variableDictionary[item]))
            else:
                pass

        else:
            importString = "StarSolveEngine.Extensions." + self.mode + ".runner"
            print("importing: " + importString)
            importedModule = importlib.import_module(importString)
            print("successfully imported... calling runner")
            importedModule.runner(self.rawData)
            print("returned from runner")
        return self.outputTexts