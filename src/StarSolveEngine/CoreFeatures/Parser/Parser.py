import re


class ParseRawData():
    
    varDict = {}
    funcList = []

    functionRegex = "#([\w]+)\((.*)\)#"
    variableRegex = "([a-zA-Z0-9]+) ?= (.*)"

    def ParseForFunctionAndVariableData(self, data):
        functionMatches = re.findall(self.functionRegex, data, re.DOTALL)
        variableMatches = re.findall(self.variableRegex, data)

        for group in functionMatches:
            print(group)
            self.funcList.append((group[0], group[1]))

        for group in variableMatches:
            if(re.match("[a-zA-Z]", group[0])):
                self.varDict[group[0]] = group[1]
            else:
                self.varDict[group[1]] = group[0]
        
    def GetFunctionList(self):
        return self.funcList

    def clear(self):
        self.funcList = []
        self.varDict = {}

    def GetVariableDictionary(self):
        print(self.varDict)
        return self.varDict

    def Clear(self):
        self.varDict = {}
        self.funcList = []
