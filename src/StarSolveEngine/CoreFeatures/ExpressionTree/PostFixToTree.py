import re


class PostFixToTree():

    nodeStack = []

    def handle(self, data):
        for item in data:
            if(re.match("[0-9]+", item) != None):
                self.nodeStack.append(Node(item))
            elif (re.match("[+-/*^]", item) != None):

                if(item == '+'):
                    self.nodeStack.append(AddNode(self.nodeStack.pop(), self.nodeStack.pop()))
                elif(item == '-'):
                    self.nodeStack.append(SubNode(self.nodeStack.pop(), self.nodeStack.pop()))
                elif(item == '/'):
                    self.nodeStack.append(DivNode(self.nodeStack.pop(), self.nodeStack.pop()))
                elif(item == '*'):
                    self.nodeStack.append(MulNode(self.nodeStack.pop(), self.nodeStack.pop()))
                elif(item == '^'):
                    self.nodeStack.append(PowNode(self.nodeStack.pop(), self.nodeStack.pop()))
            else:
                print("error handling")
                raise ValueError('Unexpected Maths encountered in core functionality.', item)
        
        return self.nodeStack.pop()


class Node:
    def __init__(self, data):
        self.left = None
        self.right = None
        self.data = data

    def Print(self):
        print(self.data)

    def Eval(self):
        return float(self.data)


class AddNode(Node):
    def __init__(self, left, right):
        super().__init__('+')
        self.left = left
        self.right = right

    def Print(self):
        self.left.Print()
        print(self.data)
        self.right.Print()

    def Eval(self):
        return self.left.Eval() + self.right.Eval()


class SubNode(Node):
    def __init__(self, left, right):
        super().__init__('-')
        self.left = left
        self.right = right

    def Print(self):
        self.left.Print()
        print(self.data)
        self.right.Print()

    def Eval(self):
        return self.right.Eval() - self.left.Eval()


class DivNode(Node):
    def __init__(self, left, right):
        super().__init__('/')
        self.left = left
        self.right = right

    def Eval(self):
        return self.right.Eval() / self.left.Eval()


class MulNode(Node):
    def __init__(self, left, right):
        super().__init__('*')
        self.left = left
        self.right = right

    def Print(self):
        self.left.Print()
        print(self.data)
        self.right.Print()

    def Eval(self):
        return self.left.Eval() * self.right.Eval()


class PowNode(Node):
    
    def __init__(self, left, right):
        super().__init__('^')
        self.left = left
        self.right = right

    def Print(self):
        self.left.Print()
        print(self.data)
        self.right.Print()

    def Eval(self):
        return self.right.Eval() ** self.left.Eval()

