import re


class StringToPostFix():

    op       = "([+\-/*()\^])"
    variable    = "([a-zA-Z0-9]+)"

    operatorPrecedence = {
        '-': 2, 
        '+': 2, 
        '/': 3, 
        '*': 3, 
        '^': 4, 
        '(': 10, 
        ')': 0
        }

    def stringToPostfix(self, strInput):
        output = []
        operators = []
        matches = self.GetExpressionMatches(strInput)
        for match in matches:
            if(match[0] == ''):
                #would want to check self.variable dictionary at this point.
                output.append(match[1])
            elif(match[1] == ''):
                prescedence = self.operatorPrecedence[match[0]]

                while(len(operators) != 0 and self.operatorPrecedence[operators[-1]] >= prescedence and operators[-1] != '('):
                    output.append(operators.pop())

                if(match[0] == ')'):
                    while(operators[-1] != '('):
                        output.append(operators.pop())
                    operators.pop()
                else:
                    operators.append(match[0])

        while(len(operators) != 0):
            output.append(operators.pop())

        return output

    def GetExpressionMatches(self, input):
        return re.findall(self.op+"|"+self.variable, input, re.DOTALL)