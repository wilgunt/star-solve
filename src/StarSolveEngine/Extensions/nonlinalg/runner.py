print("importing dependencies for nonlinalg")
from StarSolveEngine.Extensions.nonlinalg.Parsing.Parser import *
from StarSolveEngine.Extensions.nonlinalg.Solver.Solver import *

print("finished importing nonlinalg dependencies")
class NonLinearSolveEngine:

    def __init__(self):
        """Constructor"""
        self._parser = ParseObject()
        self._raw_input = ""
        self._equations = ""
        self._variables = ""
        self._initial_guess = ""

    def _pull_data(self, input_string):
        """Simply assigned the raw input to a variable for this object, self._raw_input"""
        self._raw_input = input_string
        return True

    def _process_data(self):
        """Process data from parser. Must be ran after pull_data or will yield no results."""
        parsed_result = self._parser.get_parts(self._raw_input)
        self._equations = parsed_result.equation_parts
        self._variables = parsed_result.variable_parts
        self._initial_guess = parsed_result.guess_parts

    def run(self, raw_input):
        """This function will be called to do all the processing, and return the result for nonlinear solver"""
        # Retrieve input from user and assign it to an internal variable
        self._pull_data(raw_input)

        # Process the data. Within this we parse the data and assign results to internal variables
        self._process_data()

        # Create solver object, pass in input from _parser as a tuple to the constructor
        solver = SolverObject((self._equations, self._variables, self._initial_guess))

        # Solve the system and return
        try:
            solver_results = solver.solve_system()  # Solve the system
            solver_results = str(solver_results)  # Convert to string format to return to StarSolve engine
            # NOTE: May want to do further processing later instead of just convert to String to make it look nicer.
        except ZeroDivisionError:
            # This error occurs when initial guesses are too far off
            print("ZeroDivisionError occurred. Try to adjust your initial guess values?")
        except:
            # Runs on a generic exception
            print("An error occurred during calculation!")
        else:
            # Runs on success
            return solver_results


def runner(raw_data):
    """
    This function to be called by StarSolve engine. Takes raw_data string from GUI as input.
    Input, raw_data: input string is separated by semicolons. "Equations;Variables;InitialGuess;"
    Output, return result: String containing the results of the solver
    """
    result = ""
    print("inside the nonlinsolve extension")
    try:
        engine = NonLinearSolveEngine()
        result = engine.run(raw_data)
    except: 
        print("error")
    return result

if __name__ == "__main__":
    test_result = runner("y + x + 3, x**2 + y**2 - 17; [x,y]; [1,-1];")
    print(test_result)
