import collections


class ParseObject():

    def get_parts(self, raw_data):
        """Converts raw text into this format:
        "y + x + 3, x**2 + y**2 - 17; [x,y]; [1,-1];"

        Into:
        ['y + x + 3', ' x**2 + y**2 - 17']
        ['x', 'y']
        [1, -1]

        The returned results will be 3 lists.
        The first contains string list of the equations.
        The second contains a string list of variables used in the equations.
        The third and final list contains the intial guess values, converted from string to integers

        The results is in the format of a named tuple. Access in this fasion:
        result = get_parts("your raw data here")
        equations = result.equation_parts
        variables = result.variable_parts
        guess = result.guess_parts
        """
        # Split parts into variables
        parts = raw_data.split(";")
        equations = parts[0]
        variables = parts[1]
        variables = variables[2:-1]
        initial_guess = parts[2]
        initial_guess = initial_guess[2:-1]

        # Equations to list
        equation_parts = equations.split(",")
        equation_parts_list = []
        for part in equation_parts:
            equation_parts_list.append(part)

        # Parse variables further
        variable_parts = variables.split(",")
        variable_parts_list = []
        for part in variable_parts:
            variable_parts_list.append(part)

        # Parse initial guess further
        guess_parts = initial_guess.split(",")
        guess_parts_list = []
        for part in guess_parts:
            guess_parts_list.append(int(part))

        # Create named tuple for easy access
        parts_set = collections.namedtuple('nonlinear_components', ['equation_parts', 'variable_parts', 'guess_parts'])

        # Return the tuple with part lists
        return parts_set(equation_parts_list, variable_parts_list, guess_parts_list)


if __name__ == "__main__":
    parser = ParseObject()
    test = parser.get_parts("y + x + 3, x**2 + y**2 - 17; [x,y]; [1,-1];")
    print(test.equation_parts)
    print(test.variable_parts)
    print(test.guess_parts)
