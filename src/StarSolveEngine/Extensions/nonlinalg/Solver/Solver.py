from sympy import nsolve
from sympy import Symbol
import collections


class SolverObject:

    def __init__(self, data_structure):
        self.data = data_structure

    def _setup_variables(self):
        """
        Returns a named tuple containing a list of variables as strings
        as well as a dictionary of variable names => Sympy Symbol variable
        objects. 

        Access these through the .var_list and .var_dict properties.
        """
        variable_set = collections.namedtuple('variable_set', ['var_list', 'var_dict'])

        variables = []
        variables_dictionary = {} 

        for variable in self.data[1]:
            var = Symbol(variable)  # Create Sympy Symbol object
            variables.append(var)  # Append to var object list
            variables_dictionary[variable] = var  # Create dict entry for eval() solving

        return variable_set(variables, variables_dictionary)

    def _setup_guess(self):
        """Return the initial guess value."""
        return self.data[2]

    def _setup_functions(self, variables_dictionary):
        """Return list of indidually evaluated functions."""
        functions = []
        for function in self.data[0]:
            functions.append(eval(function, variables_dictionary))
        return functions

    def solve_system(self):
        """Solves a set of non-linear equations."""
        # Get the variable list + dictionary
        var_set = self._setup_variables()

        # Holds list of all Sympy Symbol object vars, used by nsolve
        variables = var_set.var_list

        # Holds Sympy Symbol objects
        # Key => .name attribute of var, such as 'x'
        # Value => Sympy Symbol object
        # This will be used with eval() to solve functions
        variables_dictionary = var_set.var_dict

        # Get the evaluated list of individual functions
        functions = self._setup_functions(variables_dictionary)

        # Get the initial guess values
        guess = self._setup_guess()

        # Solve the non linear system of equations and return
        result = nsolve((functions),(variables),(guess))
        return result

    
if __name__ == "__main__":
    var_names = ["x","y"]
    # functions = ["y + x + 3", "x**2 + y**2 - 17"]
    functions = ["y+x+3", "x**2+y**2-17"]
    initial_guess = [1,-1]

    test = SolverObject((functions, var_names, initial_guess))

    result = None
    try:
        result = test.solve_system()
    except ZeroDivisionError:  #This error occurs when initial guesses are too far off
        print("ZeroDivisionError occured. Try to adjust your initial guses values?")
    except:
        print("An error occured")

    if (result is not None):
        print(result)
