import unittest
import sys

from src.StarSolveEngine.Extensions.linalg.Parsing.Parser import *


sys.path.append("..\src")

    
class TestParserEQ(unittest.TestCase):

    def setUp(self):
        self._Parser = ParseObject()

    def test_equations_with_spaces(self):

        eq1 = "10x + 4y + 1z = 30"

        # ParseEQ Results
        parseEQResults = self._Parser.ParseEQ(eq1)

        # LHS
        assert parseEQResults[0][0]["Value"] == "10"
        assert parseEQResults[0][0]["Variable"] == "x"
        assert parseEQResults[0][0]["Maths"] == ""

        assert parseEQResults[0][1]["Value"] == "4"
        assert parseEQResults[0][1]["Variable"] == "y"
        assert parseEQResults[0][1]["Maths"] == "+"

        assert parseEQResults[0][2]["Value"] == "1"
        assert parseEQResults[0][2]["Variable"] == "z"
        assert parseEQResults[0][2]["Maths"] == "+"

        # RHS
        assert parseEQResults[1][0]["Value"] == "30"
        assert parseEQResults[1][0]["Variable"] == ""
        assert parseEQResults[1][0]["Maths"] == ""

        ##############################################

    def test_equations_no_spaces(self):

        # EQ1 no spaces
        eq1_ns = "10x+4y+1z=30"

        # ParseEQ Results
        parseEQResults = self._Parser.ParseEQ(eq1_ns)

        # LHS
        assert parseEQResults[0][0]["Value"] == "10"
        assert parseEQResults[0][0]["Variable"] == "x"
        assert parseEQResults[0][0]["Maths"] == ""

        assert parseEQResults[0][1]["Value"] == "4"
        assert parseEQResults[0][1]["Variable"] == "y"
        assert parseEQResults[0][1]["Maths"] == "+"

        assert parseEQResults[0][2]["Value"] == "1"
        assert parseEQResults[0][2]["Variable"] == "z"
        assert parseEQResults[0][2]["Maths"] == "+"

        # RHS
        assert parseEQResults[1][0]["Value"] == "30"
        assert parseEQResults[1][0]["Variable"] == ""
        assert parseEQResults[1][0]["Maths"] == ""

    def test_ParseEq_ValidEquation_ReturnsExpectedTuple(self):
        eq1 = "9x + 13y = 2"
        eq1Parse = self._Parser.ParseEQ(eq1)

        assert eq1Parse[0][0]["Value"] == "9"
        assert eq1Parse[0][0]["Variable"] == "x"
        assert eq1Parse[0][0]["Maths"] == ""

        assert eq1Parse[0][1]["Value"] == "13"
        assert eq1Parse[0][1]["Variable"] == "y"
        assert eq1Parse[0][1]["Maths"] == "+"  # Updated, no spaces now to reflect Parser changes

        assert eq1Parse[1][0]["Value"] == "2"

    def test_ParseEq_InvalidEquation_ThrowsValueError(self):
        eq = "asdf asdf "
        try:
            eqParse = self._Parser.ParseEQ(eq)
        except:
            pass
        else:
            assert False

    def test_ParseEq_MultipleDigitsOnRHS_ReturnsExpectedValue(self):
        eq = "9x + 13y = 23"
        eqParse = self._Parser.ParseEQ(eq)
        print(eqParse)
        assert eqParse[1][0]["Value"] == "23"





    # Everything below was basically TDD for older Parsing function.
    # Will have to modify for changes made. But the tests cases and
    # expected results an be re-used. Right now they're all set to
    # pass the function

    # TODO
    # Standard equation, all input given, all positives
    def test_standard_Equations(self):
        equations = [
            "1x + 1y + 1z = 6",
            "0x + 2y + 5z = 4",
            "2x + 5y + 1z = 27"
        ]

        expectedResults = [
            2.41176471,
            4.64705882,
            -1.05882352
        ]

        #actualResults = systemOfEquations(equations)
        #print(actualResults)

        #self.assertAlmostEqual(float(str(actualResults[0])[2:-2]), expectedResults[0], places=4)
        #self.assertAlmostEqual(float(str(actualResults[1])[2:-2]), expectedResults[1], places=4)
        #self.assertAlmostEqual(float(str(actualResults[2])[2:-2]), expectedResults[2], places=4)
        pass

        ########################################################################################

    # TODO
    # Standard equation with negatives, all input given, some positives & negatives. LHS+RHS remains standard.
    def test_standard_negative_Equations(self):
        equations = [
            "2x + y - 2z = 3",
            "x - y - z = 0",
            "x + y + 3z = 12"
        ]

        expectedResults = [
            2.50000000,
            1.00000000,
            3.50000000
        ]

        pass

    # TODO
    # Non-standard formatted equations, full input may or may not be given, some positives & negatives
    def test_non_standard_Equations(self):
        equations = [
            "y - 2z + x= 3",
            "- y + x - z = 0",
            "3z + y + x = 12"
        ]

        expectedResults = [
            2.50000000,
            1.00000000,
            3.50000000
        ]

        ########################################################################################

    # TODO
    # Non-standard formatted equations, LHS & RHS may be swapped. Variables not in alphabetical order
    def test_LHS_RHS_Swaps(self):
        equations = [
            "y = 2x + 4",
            "y + 3x = 9"
        ]

        expectedResults = [
            1.00000000,
            6.00000000
        ]
        pass

    # TODO
    # Equations may have arithmetic that isn't fully solved.
    def test_non_reduced_Equations(self):
        equations = [
            "y - 1 = 2x + 3",
            "-3 + 3x + y = 6"
        ]

        expectedResults = [
            1.00000000,
            6.00000000
        ]
        pass

if __name__ == '__main__':
    unittest.main()

