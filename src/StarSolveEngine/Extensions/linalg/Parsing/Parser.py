import re


# Regex explanation
# Groups
# 1 number variable math
# 2 number variable
# 3 number (DEFAULT(NONE))
# 4 variable (DEFAULT(NONE))
# 5 math (DEFAULT(NONE))
# ((([0-9]*)([a-zA-Z]*))( ?[\+\-\*/x]? ?)

# Splits equation into left/right tuple and parses out numbers/variables/math
# eq : equation to parse
# returns: tuple containing the left and right side of the equation has parsed groups.


class ParseObject():
    # properties
    leftSidePattern = "(((-?[0-9]*)([a-zA-Z]*))( ?[\+\-\*x]? ?))"
    variableList = []
    parsedEqList = []
    eqList = []
    formattedData = ()
    variableDictionary = {}

    # clear data from the parser object
    def clear(self):
        self.variableList = []
        self.parsedEqList = []
        self.eqList = []
        self.formattedData = ()
        self.variableDictionary = {}

    # parses individual equations into a LHS and a RHS
    # and groups them by variable, value, and math
    def ParseEQ(self, eq):
        # split by '='
        leftSide, rightSide = eq.split("=")

        # group the lhs and rhs
        leftSideMatches = re.findall(self.leftSidePattern, leftSide)
        rightSideMatches = re.findall("(-?[0-9]+)", rightSide)

        left = []
        right = []

        # Flip the equation if need be.
        if re.search("[a-zA-Z]", rightSide) != None and re.search("[a-zA-Z]", leftSide) == None:
            temp = leftSide
            leftSide = rightSide
            rightSide = temp

        # dummy variable to group math with variable after it, rather than before
        temp = ""
        for group in leftSideMatches:

            variable = group[3].strip()
            value = group[2].strip()
            maths = group[4].strip()

            #print(variable, value, maths)

            # if no value but has a variable set group[2] to 1
            if variable != '' and value == '':
                value = '1'

            if variable not in self.variableList and variable != '':
                self.variableList.append(variable)

            #if (group[2] != "" and group[3] != ""):
            #if (group[2] != "" and group[3] != "" and group[4] != ""):
            if (variable != ""):
                left.append({
                    "Value": group[2],
                    "Variable": group[3],
                    "Maths": temp
                })

            temp = group[4]
            temp = temp.strip()  # This will remove spaces on either side

        right.append({
            "Value": rightSideMatches[0],
            "Variable": "",
            "Maths": ""
        })

        return (left, right)

    # function that does parsing. this is called outside
    def Parse(self):
        # parse individual equations AND load parsed equations into their own list
        for eq in self.eqList:
            temp = self.ParseEQ(eq)
            self.loadVariables(temp)
            self.parsedEqList.append(temp)

        # used to store raw data. this will be formatted later
        dataList = []

        # for every parsed equation
        for item in self.parsedEqList:
            # each equation will be represented by a single dictionary of {'variable': value,...}
            eqDict = {}
            for group in item[0]:
                # if we encounter a negative, we need to multiply the value by -1. cast as an integer for formatting purposes
                if group["Maths"] == " - ":
                    eqDict[group["Variable"]] = int(group["Value"]) * -1

                else:
                    eqDict[group["Variable"]] = int(group["Value"])

            # begin formatting... use tuple(equationDict, answerValue)
            # eqDict will have {'variable':value, ... }
            # answer value will have the answer to the equation represented by eqDict
            for group in item[1]:
                val = group["Value"]
                data = (eqDict, val)
                # stores a list of tuples
                dataList.append(data)

        # format the raw data and store it
        self.formatData(dataList)

    # adds an equations to the property: eqList
    def addEq(self, eq):
        self.eqList.append(eq)

    # returns formatted data to caller
    def getFormattedData(self):
        return self.formattedData

    # loads variables into a dictionary
    def loadVariables(self, equation):

        LHS = equation[0]
        RHS = equation[1]
        for item in LHS:
            self.variableDictionary[item['Variable']] = item['Value']

    # formats data to plug directly into numpy
    def formatData(self, dataList):
        # these are used to format the data to plug directly into numpy
        # numpy accepts data in the form Ab = x
        formattedEqs = []  # A
        formattedAnswers = []  # b

        # here we format data (i.e, filling in zeros for values not in the equations)
        for entry in dataList:
            t = []
            for v in self.variableList:
                # if the key exists, use the value associated with that key in the eqDict
                if v in entry[0]:
                    t.append(entry[0][v])
                # if the key doesnt exist, variable did not exist in equation. so use a 0
                else:
                    t.append(0)

            # append the equations and answers to their respective lists and repeat
            formattedAnswers.append(int(entry[1]))
            formattedEqs.append(t)

            # equations, answers, variable ordering
        self.formattedData = (formattedEqs, formattedAnswers, self.variableList)
