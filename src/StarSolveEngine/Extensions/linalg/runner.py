import os
import numpy as np

from StarSolveEngine.Extensions.linalg.Parsing.Parser import ParseObject
from StarSolveEngine.Extensions.linalg.Solver.Solver import SolverObject


class linsolveEngine():

    _Parser = ParseObject()
    rawInput = ""
    eqList = []
    
    def loadEquations(self):
        self.formatRawInput()
        
        for eq in self.eqList:
            if eq == "":
                pass
            else:
                self._Parser.addEq(eq)
  
    def formatRawInput(self):
        if self.rawInput == "":
            print("ERR")

        self.eqList = self.rawInput.split("\n")

    def pullData(self, inputString):
        self.rawInput = inputString
        return True

    def clear(self):
        self.eqList = []
        self.rawInput = ""
        self._Parser.clear()

    def run(self):
        self.loadEquations()
        self._Parser.Parse()

        solverData = self._Parser.getFormattedData()
        _Solver = SolverObject(solverData)
        results = _Solver.solveSystem()

        return results

def runner(rawdata):
    outputtext = ""
    try:
        engine = linsolveEngine()
        engine.pullData(rawdata)
        results = engine.run()

        for key in results:
                text = str(key) + ' = ' + str(results[key]) + '\n'
                outputtext += text
    except:
        print("linsolve error")
    return outputtext
    
if __name__ == "__main__":
    runner("1x + 1y = 6\n-3x + 1y = 2\n")
