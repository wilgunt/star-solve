import numpy as np

class SolverObject():

    def __init__(self,dataStructure):
        self.data = dataStructure

    #solves the system
    def solveSystem(self):
        #set up A (value matrix) and b (answer matrix)
        matrix_A_rows = np.array(self.data[0])
        matrix_B_rows = np.array(self.data[1])

        #solve the system and conver the answers to list for easy access
        solution = np.linalg.solve(matrix_A_rows,matrix_B_rows)
        solution = list(solution)

        #store all variables paired with their values in a dictionary and return to UI
        solutionDict = {}
        r = len(self.data[2])
        v = self.data[2]
        
        for i in range(r):
            key = v[i]
            value = round(solution[i],4)
            solutionDict[key] = value

        return solutionDict



        
        
