

from GUI.MainWindow_Editable import *
# from StarSolve.StarSolveEngine.Engine import *
# from StarSolve.StarSolveEngine.Parsing.Parser import *

# ENTRY POINT OF THE PROGRAM

if __name__ == '__main__':

    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
